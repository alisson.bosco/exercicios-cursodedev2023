public class Exercicio1 {

    public static void main( String[] args ) {
        int num1 = 5;
        int num2 = 10;

        resultadoDaOperacao( num1, num2 );
    }

    public static void resultadoDaOperacao( int num1, int num2 ) {
        int soma = num1 + num2;
        int subtracao = num1 - num2;
        int divisao = num1 / num2;
        int multiplicacao = num1 * num2;
        int resto = num1 % num2;

        System.out.println( "O resultado soma dos numeros é " + soma );
        System.out.println( "O resultado da subtração é " + subtracao );
        System.out.println( "O resultado da divisão é " + divisao );
        System.out.println( "O resultado da multiplicação é " + multiplicacao );
        System.out.println( "O resultado do resto é " + resto );
    }
}