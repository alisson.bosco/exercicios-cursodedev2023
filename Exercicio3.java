
public class Exercicio3 {

    public static void main( String[] args ) {
        String nomeComposto = "PaulosJAVA";

        if ( nomeComposto.length() >= 10 && nomeComposto.toLowerCase().contains( "java" ) ) {
            System.out.println( "Sim" );
        } else {
            System.out.println( "Não" );
        }
    }
}
