import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Exercicio4 {

    public static void main(String[] args) {
        List<String> lista = new ArrayList<>();
        lista.add("javascript");
        lista.add("typescript");
        lista.add("c++");
        lista.add("java");
        lista.add("rust");
        lista.add("ruby");
        lista.add("python");
        lista.add("lua");
        lista.add("go");

        System.out.println("O Tamanho da lista: " + lista.size());

        // Utiliza o for
        boolean contemJava = false;
        for (String linguagem : lista) {
            if (linguagem.equals("java")) {
                contemJava = true;
            }
        }

        // Utiliza o stream
        boolean temJava = lista.stream().anyMatch(linguagem -> linguagem.equals("java"));
        
         if (contemJava && temJava) {
            System.out.println("Java é o melhor");
        } else {
            System.out.println("Faltou o melhor");
        }

        List<String> novaListaOrdenada = new ArrayList<>(lista);
        Collections.sort(novaListaOrdenada);
        System.out.println("Nova lista ordenada alfabeticamente: " + novaListaOrdenada);
    }
}
